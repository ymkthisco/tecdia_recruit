'use strict';

//modaal 不要な場合は削除
const modaal = require('modaal');

/*
 * ページ内リンクスクロール
 */
$.fn.pageScroll = function (options) {
	return $(this).each(function () {
		new $.pageScroll(this, $.extend({
			'position': { pc: 0, sp: 0 },//停止位置の調整
			'fnc': undefined,//beforeScroll関数
		}, options));
	});
};
$.pageScroll = function (elem, options) {
	this.$elem = $(elem);
	this.options = options;
	this.fnc = this.options['fnc'];
	this.afterfnc = this.options['afterfnc'];
	this.href = this.$elem.attr("href");
	this.href = this.href == "#" || this.href == "" ? 'html' : this.href;
	if (this.href.split('#').length !== 1) {
		this.href = '#' + this.href.split('#')[1];
	} else if (this.href != 'html') {
		return true;
	}
	this.speed = 500;
	this.setEvent();
};
$.pageScroll.prototype = {
	setEvent: function () {
		var self = this;
		self.$elem.click(function () {
			if (self.fnc !== undefined) {
				self.fnc();
			}
			return self.startScroll();
		});
	},
	startScroll: function () {
		var self = this;
		this.target = $(this.href);
		if (this.target.length > 0) {
			this.options.position = window.innerWidth > 767 ? this.options.position.pc : this.options.position.sp;
			this.position = this.target.offset().top - this.options.position;
			$('html, body').animate({ scrollTop: this.position }, self.speed, 'swing');
			return false;
		} else {
			return true;
		}
	}
};
/*
 * 画面内に要素を固定、一定要素まで来たらそこへ固定（pagetopなどに使用）
 */
$.fn.fixedElement = function (options) {
	return $(this).each(function () {
		new fixedElement(this, $.extend({
			'position': 'bottom',//停止位置の調整
			'fixedelement': 'footer',//停止させたい要素名
			'adjust': { window: 768, num: 10 }//調整値
		}, options));
	});
};
class fixedElement {
	constructor(elem, options) {
		this._$btn = $(elem);
		this._options = options;
		this._BottomPos = parseInt(this._$btn.css('bottom'));
		$(window).on('scroll', () => {
			this.chkPos();
		});
	}
	chkPos() {
		const WindowHeight = window.innerHeight;
		const PageHeight = $(document).height();
		const ScrollTop = $(window).scrollTop();
		const footerHeight = $(this._options.fixedelement).height();
		const MoveTopBtn = WindowHeight + ScrollTop + footerHeight - PageHeight;
		let adjust = !window.matchMedia('(max-width: ' + this._options.adjust.window + 'px)').matches ? this._options.adjust.num : 0;

		if (this._options.position == 'top') {
			adjust = adjust + this._$btn.height();
		} else if (this._options.position == 'center') {
			adjust = adjust + (this._$btn.height() / 2);
		}
		if ((ScrollTop > 200) && !this._$btn.hasClass('show')) {
			this._$btn.addClass('show');
		} else if ((ScrollTop < 200) && this._$btn.hasClass('show')) {
			this._$btn.removeClass('show');
		}

		const chkfooter = ScrollTop >= PageHeight - WindowHeight - footerHeight + this._BottomPos - adjust;
		if (chkfooter) {
			this._$btn.css({ bottom: MoveTopBtn + adjust });
		} else {
			this._$btn.css({ bottom: this._BottomPos });
		}
	}

}
//URLにハッシュがついていればその位置までアニメーションでスクロールする
//オプションでPC/SP時に固定ヘッダ分ずらす数値を設定可能
//options:{position: {pc: number, sp: number }}
class scrollAfterLoading {
	constructor(options) {
		const hash = window.location.hash;

		$('html, body').css('visibility', 'hidden');//一旦非表示に
		if (hash.length < 2) {
			$('html, body').css('visibility', '');
		} else {
			setTimeout(function () {
				if (options.position) {
					options.position = window.innerWidth > 767 ? options.position.pc : options.position.sp;
				}
				$('html, body').scrollTop(0).css('visibility', '').delay(500).queue(function () {
					var position = $(hash).offset().top - options.position;
					$('html, body').animate({ scrollTop: position }, 1500, 'swing').dequeue();
				});
			}, 0);
		}
	}
}

//文字サイズ変更
class changeFontSize {
	constructor() {
		const $btn = $('.fontsizechange__link');
		$('.fontsizechange__link').on('click', function (e) {
			e.preventDefault();
			$btn.removeClass('fontsizechange__link--focus');
			$(this).addClass('fontsizechange__link--focus');
			$('html').removeClass().addClass($(this).data('fontSize')).trigger('fontSize.change');

			$.cookie('fontSize', $(this).data('fontSize'));
		});
		if ($.cookie('fontSize') !== void 0) {
			$('.fontsizechange__link[rel="' + $.cookie('fontSize') + '"]').trigger('click');
		};
	}
}

//ローディング
class pageloading {
	constructor() {
		$('body').append('<div class="loading"></div>').trigger('pageLoad.start');
		const startOnLoadImage = this.startOnLoadImage;
		const $img = $('img');
		startOnLoadImage($img).done(() => {
			this.loaderClose();
		});
	}
	loaderClose() {
		$(".loading").fadeOut('slow');
		$('.wrap').addClass('show');
		setTimeout(() => { $('body').trigger('pageLoad.loaded') }, 100);
	}
	startOnLoadImage($target) {
		var d = new $.Deferred();
		var loaded = 0;
		var max = $target.length;
		$target.each(function () {
			var targetObj = new Image();
			$(targetObj).on('load', function () {
				loaded++;
				if (loaded == max) {
					setTimeout(function () {
						d.resolve();
					}, 2000);
				}
			});
			targetObj.src = this.src;
		});
		return d.promise();
	};
}


//フォーム周り
class formSetting {
	constructor() {
		this.$form = $('.c-form');
		//アクセシビリティ対策
		//this.$form.find('.c-form__selectwrapper').attr('tabindex', 0);
		this.$form.on('focus', 'select', (e) => {
			$(e.target).parent().addClass("c-form__selectwrapper--focus");
		}).on('blur', 'select', (e) => {
			$(e.target).parent().removeClass("c-form__selectwrapper--focus");
		}).on('focus', '.c-form__radiowrapper > input[type="radio"]', (e) => {
			$(e.target).parent().addClass("c-form__radiowrapper--focus");
		}).on('blur', '.c-form__radiowrapper > input[type="radio"]', (e) => {
			$(e.target).parent().removeClass("c-form__radiowrapper--focus");
		}).on('focus', '.c-form__checkwrapper > input[type="checkbox"]', (e) => {
			$(e.target).parent().addClass("c-form__checkwrapper--focus");
		}).on('blur', '.c-form__checkwrapper > input[type="checkbox"]', (e) => {
			$(e.target).parent().removeClass("c-form__checkwrapper--focus");
		});
	}

}


//SPメニュー
class spMenu {
	constructor() {
		this._btn = $('a[data-spmenu]');
		this._scrollpos;
		this._menuopen;

		this._btn.on('click', () => {
			this.ctrlMenu();
		});
	}
	ctrlMenu() {
		this._menuopen = $('body').hasClass('activemenu');
		if (!this._menuopen) {//メニュー表示
			this._scrollpos = $(window).scrollTop();//スクロール位置を格納
			$('body').addClass('activemenu').trigger('spMenu.open');
		} else {//メニュー閉じる
			$('body').removeClass('activemenu').trigger('spMenu.close');
			$(window).scrollTop(this._scrollpos)
		}
		this._btn.toggleClass('active');
	}
}

(() => {
	$('.footer__pagetop__link').fixedElement({ fixedelement: '.footer__wrapper', position: 'bottom', adjust: { window: '1px', num: -20 } }).pageScroll();

	new formSetting();

	//new scrollAfterLoading();

	//modaal　不要な場合は削除
	$('*[js-modaal]').modaal({
		type: 'image'
	});

})();


(function($) {
  var $nav   = $('#navArea');
  var $btn   = $('.toggle_btn');
  var $mask  = $('#mask');
  var open   = 'open'; // class
  // menu open close
  $btn.on( 'click', function() {
    if ( ! $nav.hasClass( open ) ) {
      $nav.addClass( open );
    } else {
      $nav.removeClass( open );
    }
  });
  // mask close
  $mask.on('click', function() {
    $nav.removeClass( open );
  });
} )(jQuery);

