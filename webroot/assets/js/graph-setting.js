(function () {
  function r(e, n, t) {
    function o(i, f) {
      if (!n[i]) {
        if (!e[i]) {
          var c = "function" == typeof require && require;
          if (!f && c) return c(i, !0);
          if (u) return u(i, !0);
          var a = new Error("Cannot find module '" + i + "'");
          throw a.code = "MODULE_NOT_FOUND", a;
        }

        var p = n[i] = {
          exports: {}
        };
        e[i][0].call(p.exports, function (r) {
          var n = e[i][1][r];
          return o(n || r);
        }, p, p.exports, r, e, n, t);
      }

      return n[i].exports;
    }

    for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) o(t[i]);

    return o;
  }

  return r;
})()({
  1: [function (require, module, exports) {
    //値をグラフに表示させる
    Chart.plugins.register({
      afterDatasetsDraw: function (chart, easing) {
        var ctx = chart.ctx;
        chart.data.datasets.forEach(function (dataset, i) {
          var meta = chart.getDatasetMeta(i);

          if (!meta.hidden) {
            meta.data.forEach(function (element, index) {
              // 値の表示
              ctx.fillStyle = '#000'; //文字の色

              var fontSize = 20; //フォントサイズ

              var fontStyle = 'normal'; //フォントスタイル

              var fontFamily = 'Arial'; //フォントファミリー

              ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);
              var dataString = dataset.data[index].toString(); // 値の位置

              ctx.textAlign = 'end'; //テキストを中央寄せ

              ctx.textBaseline = 'middle'; //テキストベースラインの位置を中央揃え

              var padding = 0; //余白

              var position = element.tooltipPosition();
              ctx.fillText(dataString, position.x, position.y - fontSize / 2 - padding);
            });
          }
        });
      }
    }); //=========== 円グラフ ============//

    $('#chart01').on('inview', function (event, isInView) {
      //画面上に入ったらグラフを描画
      if (isInView) {
        var ctx = document.getElementById("chart01"); //グラフを描画したい場所のid

        var chart = new Chart(ctx, {
          type: 'pie',
          //グラフのタイプ
          data: {
            //グラフのデータ
            labels: ["20代", "30代", "40代", "50代"],
            //データの名前
            datasets: [{
              label: "職種別比率",
              //グラフのタイトル
              backgroundColor: ["#86CC32", "#ADCC32", "#D1F493", "#E6FFC9"],
              //グラフの背景色
              data: ["38", "19", "20", "9"] //データ

            }]
          },
          options: {
            //グラフのオプション
            maintainAspectRatio: false,
            //CSSで大きさを調整するため、自動縮小をさせない
            legend: {
              display: false //グラフの説明を表示

            },
            plugins: {
              datalabels: {
                color: '#000',
                font: {
                  weight: 'bold',
                  size: 20
                },
                formatter: (value, ctx) => {
                  let label = ctx.chart.data.labels[ctx.dataIndex];
                  return label + '\n' + value + '%';
                }
              }
            },
            tooltips: {
              //グラフへカーソルを合わせた際の詳細表示の設定
              callbacks: {
                label: function (tooltipItem, data) {
                  return data.labels[tooltipItem.index] + ": " + data.datasets[0].data[tooltipItem.index] + "%"; //%を最後につける
                }
              }
            },
            title: {
              //上部タイトル表示の設定
              display: false,
              fontSize: 10,
              text: '単位：%'
            }
          }
        });
      }

      if (isInView) {
        var ctx = document.getElementById("chart02"); //グラフを描画したい場所のid

        var chart = new Chart(ctx, {
          type: 'pie',
          //グラフのタイプ
          data: {
            //グラフのデータ
            labels: ["経験あり", "経験なし"],
            //データの名前
            datasets: [{
              label: "職種別比率",
              //グラフのタイトル
              backgroundColor: ["#86CC32", "#ADCC32"],
              //グラフの背景色
              data: ["29", "71"] //データ

            }]
          },
          options: {
            //グラフのオプション
            maintainAspectRatio: false,
            //CSSで大きさを調整するため、自動縮小をさせない
            legend: {
              display: false //グラフの説明を表示

            },
            plugins: {
              datalabels: {
                color: '#000',
                font: {
                  weight: 'bold',
                  size: 20
                },
                formatter: (value, ctx) => {
                  let label = ctx.chart.data.labels[ctx.dataIndex];
                  return label + '\n' + value + '%';
                }
              }
            },
            tooltips: {
              //グラフへカーソルを合わせた際の詳細表示の設定
              callbacks: {
                label: function (tooltipItem, data) {
                  return data.labels[tooltipItem.index] + ": " + data.datasets[0].data[tooltipItem.index] + "%"; //%を最後につける
                }
              }
            },
            title: {
              //上部タイトル表示の設定
              display: false,
              fontSize: 10,
              text: '単位：%'
            }
          }
        });
      }
    }); //=========== 円グラフ ============//

    $('#chart02').on('inview', function (event, isInView) {
      //画面上に入ったらグラフを描画
      if (isInView) {
        var ctx = document.getElementById("chart02"); //グラフを描画したい場所のid

        var chart = new Chart(ctx, {
          type: 'pie',
          //グラフのタイプ
          data: {
            //グラフのデータ
            labels: ["経験あり", "経験なし"],
            //データの名前
            datasets: [{
              label: "職種別比率",
              //グラフのタイトル
              backgroundColor: ["#86CC32", "#DDFFB5"],
              //グラフの背景色
              data: ["71", "29"] //データ

            }]
          },
          options: {
            //グラフのオプション
            maintainAspectRatio: false,
            //CSSで大きさを調整するため、自動縮小をさせない
            legend: {
              display: false //グラフの説明を表示

            },
            plugins: {
              datalabels: {
                color: '#000',
                font: {
                  weight: 'bold',
                  size: 20
                },
                formatter: (value, ctx) => {
                  let label = ctx.chart.data.labels[ctx.dataIndex];
                  return label + '\n' + value + '%';
                }
              }
            },
            tooltips: {
              //グラフへカーソルを合わせた際の詳細表示の設定
              callbacks: {
                label: function (tooltipItem, data) {
                  return data.labels[tooltipItem.index] + ": " + data.datasets[0].data[tooltipItem.index] + "%"; //%を最後につける
                }
              }
            },
            title: {
              //上部タイトル表示の設定
              display: false,
              fontSize: 10,
              text: '単位：%'
            }
          }
        });
      }
    });
  }, {}]
}, {}, [1]);